package Model;

import java.util.Date;
import java.util.Vector;

public class Hospital {
	private int inoculnum;
	private String inoculname;
	
	public Hospital(int inoculnum, String inoculname) {
		super();
		this.inoculnum = inoculnum;
		this.inoculname = inoculname;
	}

	public int getInoculnum() {
		return inoculnum;
	}
	
	public void setInoculnum(int inoculnum) {
		this.inoculnum = inoculnum;
	}
	
	public String getInoculname() {
		return inoculname;
	}
	
	public void setInoculname(String inoculname) {
		this.inoculname = inoculname;
	}
	
	public Vector<String> toVector() {
		Vector<String> r = new Vector<>();
		r.add(String.valueOf(inoculnum));
		r.add(inoculname);
		
		return r;
	}
}
