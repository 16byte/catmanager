package Model;

import java.util.Vector;

public class SellingCatModel {
	private Vector<SellingCat> sellingCats = new Vector<>();
	
	public SellingCatModel() {
		
	}
	
	public boolean add(SellingCat item) {
		if (item == null)
			return false;
		
		return sellingCats.add(item);
	}
}
