package Model;

import java.util.Vector;

public class CatModel {
	private Vector<Cat> cats = new Vector<>();
	
	public CatModel() {
		
	}
	
	public boolean add(Cat item) {
		if (item == null)
			return false;
		
		return cats.add(item);
	}
}
