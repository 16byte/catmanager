package Model;

import java.util.Vector;

public class AdopterModel {
	private Vector<Adopter> adopters = new Vector<>();

	public AdopterModel() {
		super();
	}
	
	public int getIndexWithId(String id) {
		int index = -1;
		
		for (int i=0; i<adopters.size(); ++i) {
			if (adopters.get(i).getRrnumber().equals((String)id))
				return (index = i);
		}
		
		return index;
	}
	
	public boolean updateElementsWithId(String id, String name, String address, String call) {
		for (Adopter ad : adopters) {
			if (ad.getRrnumber().equals(id)) {
				ad.setName(name);
				ad.setAddress(address);
				ad.setCallnumber(call);
				return true;
			}
		}
		
		return false;
	}
	
	public boolean add(Adopter item) {
		if (item == null)
			return false;
		
		return adopters.add(item);
	}
	
	public void del(int index) {
		if (index < 0)
			return;
		
		adopters.removeElementAt(index);
	}
}
