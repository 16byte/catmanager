package Model;

import java.util.Vector;

public class CatVaccinModel {
	private Vector<CatVaccin> models = new Vector<>();
	
	public CatVaccinModel() {
		
	}
	
	public boolean add(CatVaccin e) {
		if (e == null)
			return false;
		
		return models.add(e);
	}
}
