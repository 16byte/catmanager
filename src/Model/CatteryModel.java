package Model;

import java.util.HashMap;
import java.util.Vector;

public class CatteryModel {
	private Vector<Cattery> catteries = new Vector<>();
	
	public CatteryModel() {
		
	}
	
	public boolean add(Cattery value) {
		if (value == null)
			return false;
		
		return catteries.add(value);
	}
	
	public Cattery getValue(int id) {
		return catteries.get(id);
	}
	
	public Cattery getValueAt(int index) {
		System.out.println("Index = " + index);
		if (index < 0)
			throw new IndexOutOfBoundsException();
		
		return catteries.get(index);
	}
}
