package Model;

import java.util.Date;

public class SellingCat {
	private int catnum;
	private int scid;
	private String passport;
	private Date sellingDate;
	private String idnum;
	private String transmethod;
	private String tcompany;
	private Date tdepart;
	private Date tarriavl;
	private String tprice;
	
	public SellingCat(int catnum, int scid, String passport, Date sellingDate, String idnum, String transmethod,
			String tcompany, Date tdepart, Date tarriavl, String tprice) {
		super();
		this.catnum = catnum;
		this.scid = scid;
		this.passport = passport;
		this.sellingDate = sellingDate;
		this.idnum = idnum;
		this.transmethod = transmethod;
		this.tcompany = tcompany;
		this.tdepart = tdepart;
		this.tarriavl = tarriavl;
		this.tprice = tprice;
	}

	public int getScid() {
		return scid;
	}

	public void setScid(int scid) {
		this.scid = scid;
	}

	public String getIdnum() {
		return idnum;
	}

	public void setIdnum(String idnum) {
		this.idnum = idnum;
	}

	
	public int getCatnum() {
		return catnum;
	}

	public void setCatnum(int catnum) {
		this.catnum = catnum;
	}

	public String getPassport() {
		return passport;
	}

	public void setPassport(String passport) {
		if (passport == null)
			passport = "";
		
		this.passport = passport;
	}

	public Date getSellingDate() {
		return sellingDate;
	}

	public void setSellingDate(Date sellingDate) {		
		this.sellingDate = sellingDate;
	}

	public String getTransmethod() {
		return transmethod;
	}

	public void setTransmethod(String transmethod) {
		if (transmethod == null)
			transmethod = "";
			
		this.transmethod = transmethod;
	}

	public String getTcompany() {
		return tcompany;
	}

	public void setTcompany(String tcompany) {
		if (tcompany == null)
			tcompany = "";
		
		this.tcompany = tcompany;
	}

	public Date getTdepart() {
		return tdepart;
	}

	public void setTdepart(Date tdepart) {
		this.tdepart = tdepart;
	}

	public Date getTarriavl() {
		return tarriavl;
	}

	public void setTarriavl(Date tarriavl) {
		
		this.tarriavl = tarriavl;
	}

	public String getTprice() {
		return tprice;
	}

	public void setTprice(String tprice) {
		if (tprice == null)
			tprice = "";
		
		this.tprice = tprice;
	}
	
	@Override
	public String toString() {
		// TODO Auto-generated method stub
		return String.valueOf(this.getCatnum());
	}
	


}
