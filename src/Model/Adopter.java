package Model;

import java.util.Vector;

public class Adopter extends AbstractModel {
	private String rrnumber;
	private String name;
	private String address;
	private String callnumber;
	
	public Adopter(String rrnumber, String name, String address, String callnumber) {
		super();
		this.rrnumber = rrnumber;
		this.name = name;
		this.address = address;
		this.callnumber = callnumber;
	}
	
	public String getRrnumber() {
		return rrnumber;
	}
	public void setRrnumber(String rrnumber) {
		this.rrnumber = rrnumber;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getAddress() {
		return address;
	}
	public void setAddress(String address) {
		this.address = address;
	}
	public String getCallnumber() {
		return callnumber;
	}
	public void setCallnumber(String callnumber) {
		this.callnumber = callnumber;
	}
	
	public Vector<String> toVector() {
		Vector<String> vec = new Vector<>();
		vec.add(rrnumber);
		vec.add(name);
		vec.add(address);
		vec.add(callnumber);		
		return vec;
	}
	
	
}
