package Model;

import java.util.Vector;

public class HospitalModel {
	private Vector<Hospital> hospitals = new Vector<>();
	
	public HospitalModel() {
		
	}

	public boolean add(Hospital e) {
		if (e == null)
			return false;
		
		return hospitals.add(e);
	}
}
