package Model;

import java.util.Date;

public class Cat {
	private int catnumber;
	private String name;
	private Date birthday;
	private String sex;
	private String kind;
	private String color;
	private String cost;
	private Date supplydate;
	private int companyNum;
	
	public Cat(int catnumber, String name, Date birthday, String sex, String kind, String color, String cost, Date supplydate, int company) {
		super();
		this.catnumber = catnumber;
		this.name = name;
		this.birthday = birthday;
		this.sex = sex;
		this.kind = kind;
		this.color = color;
		this.cost = cost;
		this.supplydate = supplydate;
		this.companyNum = company;
	}
	
	public Cat(Cat cat) {
		super();
		this.catnumber = cat.catnumber;
		this.name = cat.name;
		this.birthday = cat.birthday;
		this.sex = cat.sex;
		this.kind = cat.kind;
		this.color = cat.color;
		this.cost = cat.cost;
		this.supplydate = cat.supplydate;
		this.companyNum = cat.companyNum;
	}

	public Date getSupplydate() {
		return supplydate;
	}

	public void setSupplydate(Date supplydate) {
		this.supplydate = supplydate;
	}

	public int getCatnumber() {
		return catnumber;
	}
	public void setCatnumber(int cat_number) {
		this.catnumber = cat_number;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public Date getBirthday() {
		return birthday;
	}
	public void setBirthday(Date birthday) {
		this.birthday = birthday;
	}
	public String getSex() {
		return sex;
	}
	public void setSex(String sex) {
		this.sex = sex;
	}
	public String getKind() {
		return kind;
	}
	public void setKind(String kind) {
		this.kind = kind;
	}
	public String getColor() {
		return color;
	}
	public void setColor(String color) {
		this.color = color;
	}
	public String getCost() {
		return cost;
	}
	public void setCost(String cost) {
		this.cost = cost;
	}

	public int getCompanyNum() {
		return companyNum;
	}

	public void setCompanyNum(int companyNum) {
		this.companyNum = companyNum;
	}
	
	@Override
	public String toString() {
		// TODO Auto-generated method stub
		return name + "(" + String.valueOf(this.getCatnumber()) + ")";
	}
}
