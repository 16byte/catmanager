package Model;


import java.util.Date;
import java.util.Vector;

public class CatVaccin {
	private int catnum;
	private int vaccinnum;
	private Date vdate;
	
	public CatVaccin(int catnum, int vaccinnum, Date vdate) {
		super();
		this.catnum = catnum;
		this.vaccinnum = vaccinnum;
		this.vdate = vdate;
	}
	
	public int getCatnum() {
		return catnum;
	}
	public void setCatnum(int catnum) {
		this.catnum = catnum;
	}
	public int getVaccinnum() {
		return vaccinnum;
	}
	public void setVaccinnum(int vaccinnum) {
		this.vaccinnum = vaccinnum;
	}
	public Date getVdate() {
		return vdate;
	}
	public void setVdate(Date vdate) {
		this.vdate = vdate;
	}
	
	public Vector<String> toVector() {
		Vector<String> vec = new Vector<>();
		vec.add(String.valueOf(catnum));
		vec.add(String.valueOf(vaccinnum));
		vec.add(vdate.toString());
		return vec;
	}
}
