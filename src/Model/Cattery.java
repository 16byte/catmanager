package Model;

import java.util.Vector;

public class Cattery extends AbstractModel {
	private int id;
	private String name;
	private String address;
	private String callnumber;

	public Cattery(int id, String name, String address, String callnumber) {
		super();
		this.id = id;
		this.name = name;
		this.address = address;
		this.callnumber = callnumber;
	}
	
	public int getId() {
		return id;
	}
	
	public void setId(int id) {
		int oldId = this.id;
		this.id = id;
		firePropertyChange("Id", oldId, id);
	}
	
	public String getName() {
		return name;
	}
	
	public void setName(String name) {
		String oldName = this.name;
		this.name = name;
		firePropertyChange("Name", oldName, name);
	}
	
	public String getAddress() {
		return address;
	}
	
	public void setAddress(String address) {
		this.address = address;
	}
	
	public String getCallnumber() {
		return callnumber;
	}
	
	public void setCallnumber(String callnumber) {
		this.callnumber = callnumber;
	}
	
	public Vector<String> toVector() {
		Vector<String> vec = new Vector<>();
		vec.add(String.valueOf(id));
		vec.add(name);
		vec.add(address);
		vec.add(callnumber);		
		return vec;
	}
	
	@Override
	public String toString() {
		// TODO Auto-generated method stub
		return super.toString();
	}
}
