package Controller;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Vector;

import javax.swing.JOptionPane;

import Database.DbManager;
import Model.*;
import View.*;

public class SellingCatController {
	private SellingCatModel model;
	private CatPanel view;
	
	public SellingCatController(SellingCatModel m, CatPanel v) {
		this.model = m;
		this.view = v;
		
		Vector<SellingCat> rv = DbManager.getInstance().SqlGetAllSellingCat();
		if (rv != null) {
			for (SellingCat c : rv) {
				model.add(c);
				v.getScatlistModel().addElement(c);
			}
		}
		
		v.getParcelbutton().addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent arg0) {
				// TODO Auto-generated method stub
				String datePattern = "^(19|20)\\d\\d[- /.](0[1-9]|1[012])[- /.](0[1-9]|[12][0-9]|3[01])$";
				int catnum = ((Cat)v.getCatList().getSelectedValue()).getCatnumber();
				String id = v.getTextField_id().getText();
				String passport = v.getTextField_passport().getText();
				String adopter = ((String)v.getComboBoxAdoptor().getSelectedItem()).split("\\ ")[0];
				DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
				Date parcelDate = new Date();
				v.getTextField_parceldate().setText(dateFormat.format(parcelDate));
				String method = v.getTextField_method().getText();
				String tcompany = v.getTextField_tcomapny().getText();
				String tdepart = v.getTextField_tdepart().getText();
				String tarrival = v.getTextField_tarrival().getText();
				String price = v.getTextField_tprice().getText();
				
				if (id.length() <= 0 || passport.length() <= 0 || adopter.length() <= 0 || v.getTextField_parceldate().getText().length() <= 0 ||
					method.length() <= 0 || tcompany.length() <= 0 || tdepart.length() <= 0 || tarrival.length() <= 0 || price.length() <= 0) {
					JOptionPane.showMessageDialog(null, "Have to fill all of the components.");
					return;
				}
				
				if (!tdepart.matches(datePattern) || !tarrival.matches(datePattern)) {
					JOptionPane.showMessageDialog(null, "Have to fill a vaild date format");
					return;
				}
				
				try {
					SellingCat scat = new SellingCat(catnum, Integer.parseInt(id), passport, parcelDate, adopter, method, tcompany, dateFormat.parse(tdepart), dateFormat.parse(tarrival), price);
					if (DbManager.getInstance().SqlAddSellingCat(scat)) {
						v.getScatlistModel().addElement(scat);
					} else {
						scat = null;
						JOptionPane.showMessageDialog(null, "Can't connect to the database");
					}
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
		
		v.getDeletebutton().addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent e) {
				// TODO Auto-generated method stub
				SellingCat scat = (SellingCat)v.getSellingCatList().getSelectedValue();
				
				if (DbManager.getInstance().SqlDelSellingCat(scat.getCatnum(), scat.getPassport())) {
					System.out.println("Deleted");
					v.getScatlistModel().removeElement(scat);
				} else {
					JOptionPane.showMessageDialog(null, "Can't connect to the database");
				}
			}
		});
	}
	
	
}
