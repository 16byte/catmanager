package Controller;

import View.*;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.Vector;

import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;

import Database.DbManager;
import Model.*;
import Util.Pair;

public class HospitalController {
	private HospitalModel model;
	private HospitalView view;
	
	public HospitalController(HospitalModel m, HospitalView v) {
		super();
		this.model = m;
		this.view = v;
		
		Vector<Hospital> rv = DbManager.getInstance().SqlGetAllHospital();
		if (rv != null) {
			for (Hospital c : rv) {
				model.add(c);
				v.getTableModel().addRow(c.toVector());
			}
		}
		
		v.getvTable().getSelectionModel().addListSelectionListener(new ListSelectionListener() {
			
			@Override
			public void valueChanged(ListSelectionEvent e) {
				// TODO Auto-generated method stub
				if(!e.getValueIsAdjusting()){
				    int selectedRow = v.getvTable().getSelectedRow();
				    int columnCount = v.getvTable().getColumnCount();

				    if (selectedRow >= 0 && columnCount > 0) {
					    v.getVnumTxtField().setText((String) v.getvTable().getValueAt(selectedRow, 0));
					    v.getVnameTxtField().setText((String) v.getvTable().getValueAt(selectedRow, 1));
				    } else {
				    	v.getVnumTxtField().setText("");
					    v.getVnameTxtField().setText("");
				    }
				}
			}
		});
		
		v.getUpdateButton().addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent arg0) {
				// TODO Auto-generated method stub
				
				v.getRtableModel().getDataVector().removeAllElements();
				v.getRtableModel().fireTableDataChanged();
				
				Vector<Pair<String, String>> rs = DbManager.getInstance().SqlGetVaccinCount();
				if (rs != null) {
					for (Pair<String, String> s : rs) {
						v.getRtableModel().addRow(s.toVector());
					}
				}
			}
		});
	}
	
	
}
