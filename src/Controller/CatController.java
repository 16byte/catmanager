package Controller;

import Model.*;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Vector;

import javax.swing.JOptionPane;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;

import Database.*;
import View.*;

public class CatController {
	private CatModel model;
	private CatPanel view;
	
	public CatController(CatPanel v, CatModel m) {
		this.view = v;
		this.model = m;
		
		Vector<Cat> rv = DbManager.getInstance().SqlGetAllCat();
		if (rv != null) {
			for (Cat c : rv) {
				model.add(c);
				v.getListModel().addElement(c);
			}
		}
		
		Vector<Cattery> rc = DbManager.getInstance().SqlGetAllCattery();
		if (rc != null) {
			for (Cattery c : rc) {
				v.getBoxModel().addElement(String.valueOf(c.getId()) + " (" + c.getName() + ")");
			}
		}
		
		Vector<Adopter> ra = DbManager.getInstance().SqlGetAllAdopter();
		if (ra != null) {
			for (Adopter c : ra) {
				v.getAdopterBoxModel().addElement(String.valueOf(c.getRrnumber()) + " (" + c.getName() + ")");
			}
		}
		
		v.getCatList().getSelectionModel().addListSelectionListener(new ListSelectionListener() {
			
			@Override
			public void valueChanged(ListSelectionEvent e) {
				// TODO Auto-generated method stub
				if(!e.getValueIsAdjusting()) {

					Cat cat = (Cat)v.getCatList().getSelectedValue();
					
					if (cat != null) {
						v.getTextField_catnum().setText(String.valueOf(cat.getCatnumber()));
						v.getTextField_name().setText(cat.getName());
						v.getTextField_Date().setText(cat.getBirthday().toString());
						v.getTextField_gender().setText(cat.getSex());
						v.getTextField_color().setText(cat.getColor());
						v.getTextField_price().setText(cat.getCost());
						v.getTextField_species().setText(cat.getKind());
						v.getTextField_supplydate().setText(cat.getSupplydate().toString());

						int index = -1;
						for(int i = 0; i< v.getBoxModel().getSize();i++){
							if (v.getBoxModel().getElementAt(i).split("\\ ")[0].equals(String.valueOf(cat.getCompanyNum()))) {
								index = i;
								break;
							}
						}
						
						v.getCompanyComboBox().setSelectedIndex(index);
							
						
					} else {
						v.getTextField_catnum().setText("");
						v.getTextField_name().setText("");
						v.getTextField_Date().setText("");
						v.getTextField_gender().setText("");
						v.getTextField_color().setText("");
						v.getTextField_price().setText("");
						v.getTextField_species().setText("");
						v.getTextField_supplydate().setText("");
					}
				}
			}
		});
		
		v.getSellingCatList().getSelectionModel().addListSelectionListener(new ListSelectionListener() {
			
			@Override
			public void valueChanged(ListSelectionEvent e) {
				// TODO Auto-generated method stub
				if(!e.getValueIsAdjusting()) {
					DateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
					SellingCat cat = (SellingCat)v.getSellingCatList().getSelectedValue();
					
					if (cat != null) {
						v.getTextField_id().setText(String.valueOf(cat.getScid()));
						v.getTextField_passport().setText(cat.getPassport());
						v.getTextField_parceldate().setText(formatter.format(cat.getSellingDate()));
						v.getTextField_method().setText(cat.getTransmethod());
						v.getTextField_tcomapny().setText(cat.getTcompany());
						if (cat.getTdepart() != null)
							v.getTextField_tdepart().setText(cat.getTdepart().toString());
						else
							v.getTextField_tdepart().setText("");
						
						if (cat.getTarriavl() != null)
							v.getTextField_tarrival().setText(cat.getTarriavl().toString());
						else
							v.getTextField_tarrival().setText("");
						
						v.getTextField_tprice().setText(cat.getTprice());
							
						int index = -1;
						for(int i = 0; i< v.getAdopterBoxModel().getSize();i++){
							if (v.getAdopterBoxModel().getElementAt(i).split("\\ ")[0].equals(cat.getIdnum())) {
								index = i;
								break;
							}
						}
						
						v.getComboBoxAdoptor().setSelectedIndex(index);
					} else {
						v.getTextField_id().setText("");
						v.getTextField_passport().setText("");
						v.getTextField_parceldate().setText("");
						v.getTextField_method().setText("");
						v.getTextField_tcomapny().setText("");
						v.getTextField_tdepart().setText("");
						v.getTextField_tarrival().setText("");
						v.getTextField_tprice().setText("");
						v.getComboBoxAdoptor().setSelectedIndex(-1);
					}
				}
			}
		});
		
		v.getAddButton().addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent arg0) {
				// TODO Auto-generated method stub
				String datePattern = "([12]\\d{3}-(0[1-9]|1[0-2])-(0[1-9]|[12]\\d|3[01]))";
				
				String catnum = v.getTextField_catnum().getText();
				String name = v.getTextField_name().getText();
				String birth = v.getTextField_Date().getText();
				String gender = v.getTextField_gender().getText();
				String species = v.getTextField_species().getText();
				String color = v.getTextField_color().getText();
				String price = v.getTextField_price().getText();
				String supplydat = v.getTextField_supplydate().getText();
				
				boolean inputCheck = catnum.length() <= 0 || name.length() <= 0 || birth.length() <= 0 || 
						gender.length() <= 0 || species.length() <= 0 || color.length() <= 0 || price.length() <= 0 ||
						price.length() <= 0;
				if (inputCheck) {
					JOptionPane.showMessageDialog(null, "Have to fill all of the components.");
					return;
				}
				
				boolean patternCheck = birth.matches(datePattern) && supplydat.matches(datePattern);
				if (!patternCheck) {
					JOptionPane.showMessageDialog(null, "Have to input a vaild date format.");
					return;
				}
				
				String company = v.getCompanyComboBox().getSelectedItem().toString().split("\\ ")[0];
				int companyNum = Integer.parseInt(company);
				
				System.out.println("Company = " + companyNum);
				
				DateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
				try {
					Cat newCat = new Cat(Integer.parseInt(catnum), name, formatter.parse(birth), gender, species, color, price, formatter.parse(supplydat), companyNum);
					if (DbManager.getInstance().SqlAddCat(newCat)) {
						v.getListModel().addElement(newCat);
						model.add(newCat);
					} else {
						JOptionPane.showMessageDialog(null, "Can't connect to the database.");
						newCat = null;
					}
				} catch (ParseException e) {
					JOptionPane.showMessageDialog(null, "Have to fill vaild date format");
					e.printStackTrace();
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}
	
}
