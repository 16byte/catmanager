package Controller;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.Vector;

import javax.swing.JOptionPane;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;

import Controller.*;
import Database.DbManager;
import Model.*;
import Util.Pair;
import View.*;

public class CatteryController extends AbstractController  {
	private CatteryModel model;
	private CatteryPanel view;
	
	public CatteryController(CatteryModel m, CatteryPanel v) {
		// TODO Auto-generated constructor stub
		this.model = m;
		this.view = v;
		
		Vector<Cattery> rv = DbManager.getInstance().SqlGetAllCattery();
		if (rv != null) {
			for (Cattery c : rv) {
				model.add(c);
				v.getTableModel().addRow(c.toVector());
			}
		}
		
		v.getCatteryTable().getSelectionModel().addListSelectionListener(new ListSelectionListener() {
			
			@Override
			public void valueChanged(ListSelectionEvent e) {
				// TODO Auto-generated method stub
				if(!e.getValueIsAdjusting()){
				    int selectedRow = v.getCatteryTable().getSelectedRow();
				    int columnCount = v.getCatteryTable().getColumnCount();

				    if (selectedRow >= 0 && columnCount > 0) {
					    v.getCompanyTxtField().setText((String) v.getCatteryTable().getValueAt(selectedRow, 0));
					    v.getNameTxtField().setText((String) v.getCatteryTable().getValueAt(selectedRow, 1));
					    v.getAddrTxtField().setText((String) v.getCatteryTable().getValueAt(selectedRow, 2));
					    v.getCallTxtField().setText((String) v.getCatteryTable().getValueAt(selectedRow, 3));
				    } else {
				    	v.getCompanyTxtField().setText("");
					    v.getNameTxtField().setText("");
					    v.getAddrTxtField().setText("");
					    v.getCallTxtField().setText("");
				    }
				}
			}
		});
		
		v.getAddButton().addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				
				String company = v.getCompanyTxtField().getText();				
				String name = v.getNameTxtField().getText();
				String addr = v.getAddrTxtField().getText();
				String call = v.getCallTxtField().getText();

				if (company.length() <= 0 || name.length() <= 0 || addr.length() <= 0 || call.length() <= 0) {
					JOptionPane.showMessageDialog(null, "Have to fill all of the components.");
					return;
				}
				
				
/*				JOptionPane.showMessageDialog(null, "Company: " + company + " Name: " + name + " addr: " + addr + " call: " + call);
				return; */
				
				Cattery m = new Cattery(Integer.parseInt(company), name, addr, call);
				if (DbManager.getInstance().SqlAddCattery(m)) {
					model.add(m);
					v.getTableModel().addRow(m.toVector());
				} else {
					JOptionPane.showMessageDialog(null, "Can't connect to the database");
					m = null;
				}
			}
		});
		
		v.getSearchButton().addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent e) {
				// TODO Auto-generated method stub
				String q = v.getSearchTxtField().getText();
				if (q.length() <= 0) {
					JOptionPane.showMessageDialog(null, "Have to fill all of the components.");
					return;
				}
				
				String searchCategory = (String)v.getSearchComboBox().getSelectedItem();
				Vector<Cattery> rv = DbManager.getInstance().SqlGetSearchCattery(searchCategory, q);

				v.getSearchTableModel().getDataVector().removeAllElements();
				v.getSearchTableModel().fireTableDataChanged();
				
				int resultCount = 0;
				if (rv != null) {
					for (Cattery c : rv) {
						v.getSearchTableModel().addRow(c.toVector());
						resultCount++;
					}
				}
				
				String resultMsg = "총 " + resultCount + "개가 검색되었습니다.";
				v.getSearchResultLabel().setText(resultMsg);
			}
		});
		
		v.getDateButton().addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent e) {
				// TODO Auto-generated method stub
				String q = v.getDateTxtField().getText();
				
				if (q.length() <= 0) {
					JOptionPane.showMessageDialog(null, "Have to fill all of the components.");
					return;
				}
				
				// 해당 고양이가 분양된 날짜
				Vector<Pair<String, String>> rs = DbManager.getInstance().SqlGetSellingDateOfCattery(q);

				v.getCtableModel().getDataVector().removeAllElements();
				v.getCtableModel().fireTableDataChanged();
				
				if (rs != null) {
					for (Pair<String, String> c : rs) {
						v.getCtableModel().addRow(c.toVector());
					}
				}
			}
		});
	}
	
	public void changeElementId(int id) {
		setModelProperty("Id", id);
	}
	
	public void changeElementName(String name) {
		setModelProperty("Name", name);
	}
}
