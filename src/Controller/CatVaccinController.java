package Controller;

import View.*;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Vector;

import javax.swing.JOptionPane;

import Database.DbManager;
import Model.*;
import Util.Pair;

public class CatVaccinController {
	private CatVaccinModel model;
	private CatVaccinPanel view;
	
	public CatVaccinController(CatVaccinModel m, CatVaccinPanel v) {
		model = m;
		view = v;
		
		Vector<CatVaccin> rv = DbManager.getInstance().SqlGetAllCatVaccin();
		if (rv != null) {
			for (CatVaccin c : rv) {
				model.add(c);
				v.getCvTableModel().addRow(c.toVector());
			}
		}
		
		Vector<Cat> rc = DbManager.getInstance().SqlGetAllCat();
		if (rc != null) {
			for (Cat c : rc) {
				v.getCatnumBoxModel().addElement(String.valueOf(c.getCatnumber()) + " (" + c.getName() + ")");
			}
		}
		
		Vector<Hospital> rh = DbManager.getInstance().SqlGetAllHospital();
		if (rh != null) {
			for (Hospital c : rh) {
				v.getVacnumBoxModel().addElement(String.valueOf(c.getInoculnum()) + " (" + c.getInoculname() + ")");
			}
		}
		
		v.getAddButton().addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent arg0) {
				// TODO Auto-generated method stub
				String catnum = ((String)v.getCatnumBoxModel().getSelectedItem()).split("\\ ")[0];
				String vacnum = ((String)v.getVacnumBoxModel().getSelectedItem()).split("\\ ")[0];
				String date = v.getTextField_Date().getText();
				
				if (catnum.length() <= 0 || vacnum.length() <= 0 || date.length() <= 0) {
					JOptionPane.showMessageDialog(null, "Have to fill all of the components.");
					return;
				}
				
				DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
				try {
					Date vDate = dateFormat.parse(date);
					CatVaccin catVaccin = new CatVaccin(Integer.parseInt(catnum), Integer.parseInt(vacnum), vDate);
					if (DbManager.getInstance().SqlAddCatVaccin(catVaccin)) {
						model.add(catVaccin);
						v.getCvTableModel().addRow(catVaccin.toVector());
					} else {
						JOptionPane.showMessageDialog(null, "Can't connect to the database");
						catVaccin = null;
					}
					
				} catch (ParseException e) {
					// TODO Auto-generated catch block
					JOptionPane.showMessageDialog(null, "Have to fill vaild date format");
					e.printStackTrace();
				}
			}
		});
		
		v.getQueryButton().addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent arg0) {
				// TODO Auto-generated method stub
				String vaclist = v.getTextField_vaccinnum().getText();
				
				if (vaclist.length() <= 0) {
					JOptionPane.showMessageDialog(null, "Have to fill all of the components.");
					return;
				}
				
				v.getCatnumTableModel().getDataVector().removeAllElements();
				v.getCatnumTableModel().fireTableDataChanged();
				
				String[] vacinLists = vaclist.split("\\ ");
				Vector<Pair<String, String>> rs = DbManager.getInstance().SqlGetIntersectVaccin(vacinLists);
				if (rs != null) {
					for (Pair<String, String> s : rs) {
						v.getCatnumTableModel().addRow(s.toVector());
					}
				}
			}
		});
	}
	
	
}
