package Controller;

import java.awt.event.ActionEvent;

import java.awt.event.ActionListener;
import java.util.Vector;

import javax.swing.JOptionPane;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;
import javax.swing.event.TableModelEvent;
import javax.swing.event.TableModelListener;

import Database.DbManager;
import Model.*;
import View.*;
import Util.*;

public class AdopterController {
	private AdopterModel model; 
	private AdopterPanel view;
	
	public AdopterController(AdopterModel m, AdopterPanel v) {
		this.model = m;
		this.view = v;
		
		Vector<Adopter> rv = DbManager.getInstance().SqlGetAllAdopter();
		if (rv != null) {
			for (Adopter c : rv) {
				model.add(c);
				v.getTableModel().addRow(c.toVector());
			}
		}
		
		v.getAdopterTable().getSelectionModel().addListSelectionListener(new ListSelectionListener() {
			
			@Override
			public void valueChanged(ListSelectionEvent e) {
				// TODO Auto-generated method stub
				if(!e.getValueIsAdjusting()){
				    int selectedRow = v.getAdopterTable().getSelectedRow();
				    int columnCount = v.getAdopterTable().getColumnCount();

				    if (selectedRow >= 0 && columnCount > 0) {
					    v.getIdTxtField().setText((String) v.getAdopterTable().getValueAt(selectedRow, 0));
					    v.getNameTxtField().setText((String) v.getAdopterTable().getValueAt(selectedRow, 1));
					    v.getAddrTxtField().setText((String) v.getAdopterTable().getValueAt(selectedRow, 2));
					    v.getCallTxtField().setText((String) v.getAdopterTable().getValueAt(selectedRow, 3));
				    } else {
				    	v.getIdTxtField().setText("");
					    v.getNameTxtField().setText("");
					    v.getAddrTxtField().setText("");
					    v.getCallTxtField().setText("");
				    }
				}
			}
		});
		
		v.getAddButton().addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent e) {
				// TODO Auto-generated method stub
				String id = v.getIdTxtField().getText();				
				String name = v.getNameTxtField().getText();
				String addr = v.getAddrTxtField().getText();
				String call = v.getCallTxtField().getText();
				
				if (id.length() <= 0 || name.length() <= 0 || addr.length() <= 0 || call.length() <= 0) {
					JOptionPane.showMessageDialog(null, "Have to fill all of the components.");
					return;
				}
				
				Adopter adopter = new Adopter(id, name, addr, call);
				if (DbManager.getInstance().SqlAddAdopter(adopter)) {
					model.add(adopter);
					v.getTableModel().addRow(adopter.toVector());
				} else {
					JOptionPane.showMessageDialog(null, "Can't connect the database or is bad query expression.");
					adopter = null;
				}
			}
		});
		
		v.getModifyButton().addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent e) {
				// TODO Auto-generated method stub
				String id = v.getIdTxtField().getText();				
				String name = v.getNameTxtField().getText();
				String addr = v.getAddrTxtField().getText();
				String call = v.getCallTxtField().getText();
				
				if (id.length() <= 0 || name.length() <= 0 || addr.length() <= 0 || call.length() <= 0) {
					JOptionPane.showMessageDialog(null, "Have to fill all of the components.");
					return;
				}
				
			    int selectedRow = v.getAdopterTable().getSelectedRow();
			    int columnCount = v.getAdopterTable().getColumnCount();

			    if (DbManager.getInstance().SqlUpdateAdopterWithId(id, name, addr, call)) {
			    	System.out.println("updated");
			    	model.updateElementsWithId(id, name, addr, call);
			    	
				    if (selectedRow >= 0 && columnCount > 0) {
					//    v.getIdTxtField().setText((String) v.getAdopterTable().getValueAt(selectedRow, 0));
					    v.getNameTxtField().setText(name);
					    v.getAddrTxtField().setText(addr);
					    v.getCallTxtField().setText(call);
					    
					    v.getTableModel().setValueAt((String)name, selectedRow, 1);
					    v.getTableModel().setValueAt((String)addr, selectedRow, 2);
					    v.getTableModel().setValueAt((String)call, selectedRow, 3);
					    v.getTableModel().fireTableDataChanged();
				    }
			    }
			}
		});
		
		v.getDelButton().addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent e) {
				// TODO Auto-generated method stub
				String id = v.getIdTxtField().getText();
				int selectedRow = v.getAdopterTable().getSelectedRow();
				
				System.out.println("Selected Row: " + selectedRow);
				
				if (id.length() <= 0 || selectedRow < 0) {
					JOptionPane.showMessageDialog(null, "Have to fill all of the components.");
					return;
				}
				
				if (DbManager.getInstance().SqlDeleteAdopterWithId(id)) {
					int index = model.getIndexWithId(id);
					model.del(index);
					v.getTableModel().removeRow(selectedRow);
				} else {
					JOptionPane.showMessageDialog(null, "Can't connect to the database");
				}
			}
		});
		
		v.getRrnButton().addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent e) {
				// TODO Auto-generated method stub
				String rrn = v.getRrnField().getText();
				
				if (rrn.length() <= 0) {
					JOptionPane.showMessageDialog(null, "Have to fill all of the components.");
					return;
				}
				
				v.getRsTableModel().getDataVector().removeAllElements();
				v.getRsTableModel().fireTableDataChanged();
				
				Vector<Pair<String, String>> rs = DbManager.getInstance().SqlGetAllCatOfAdopter(rrn);
				if (rs != null) {
					for (Pair<String, String> r : rs) {
						v.getRsTableModel().addRow(r.toVector());
					}
				}
			}
		});
		
		v.getVaccinButton().addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent arg0) {
				// TODO Auto-generated method stub
				String rrn = v.getVaccinTextField().getText();
				
				if (rrn.length() <= 0) {
					JOptionPane.showMessageDialog(null, "Have to fill all of the components.");
					return;
				}
				
				v.getVrsTableModel().getDataVector().removeAllElements();
				v.getVrsTableModel().fireTableDataChanged();
				
				Vector<Triple<String, String, String>> rs = DbManager.getInstance().SqlGetAllCatOfAdopterOfVaccin(rrn);
				if (rs != null) {
					for (Triple<String, String, String> r : rs) {
						v.getVrsTableModel().addRow(r.toVector());
					}
				}
			}
		});
	}
	
	
}
