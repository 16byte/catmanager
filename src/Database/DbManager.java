package Database;

import java.sql.*;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Vector;

import Model.*;
import Util.*;

public class DbManager {
	private static DbManager instance = new DbManager();
	private static String URL = "jdbc:oracle:thin:@storyhost:1521:orcl";
	
	private String id;
	private String password;
	private Connection conn = null;
	
	public DbManager() {
		
	}
	
	// It must be called before using this class
	public void Initialize(String id, String password) {
		this.id = id;
		this.password = password;
	}
	
	public void Finalize() {
		try {
			conn.close();
		} catch (SQLException e) {
			
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	public void Connect() {
		try {
			Class.forName("oracle.jdbc.OracleDriver");
			conn = DriverManager.getConnection(URL, id, password);
		} catch (ClassNotFoundException  e) {
			System.out.println("ClassNotFoundException: " + e.getMessage());
		} catch (Exception e) {
			System.out.println("Exception: " + e.getMessage());
		}
	}
	
	public Vector<Cat> SqlGetAllCat() {
		Vector<Cat> cats = new Vector<>();
		String query = "select * from Cat";
		
		try {
			Statement stmt = conn.createStatement();
			ResultSet rs = stmt.executeQuery(query);
			while (rs.next()) {
				cats.add(new Cat(rs.getInt(1), rs.getString(2), rs.getDate(3), rs.getString(4), rs.getString(5), rs.getString(6), rs.getString(7), rs.getDate(8), rs.getInt(9)));
			}
		} catch (SQLException e) {
			e.printStackTrace();
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		return cats;
	}
	
	public Cat SqlGetCat(int id) {
		String query = "select * from Cat where catnum='" + id + "'";
		
		try {
			Statement stmt = conn.createStatement();
			ResultSet rs = stmt.executeQuery(query);
			
			if (rs.getRow() > 1) {
				Cat newCat = new Cat(rs.getInt(1), rs.getString(2), rs.getDate(3), rs.getString(4), rs.getString(5), rs.getString(6), rs.getString(7), rs.getDate(8), rs.getInt(9));
				return newCat;
			}
		} catch (SQLException e) {
			e.printStackTrace();
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		return null;
	}
	
	public boolean SqlAddCat(Cat cat) {
		String query = "insert into Cat values (?, ?, ?, ?, ?, ?, ?, ?, ?)";
		
		try {
			PreparedStatement pstmt = conn.prepareStatement(query);
			pstmt.setInt(1, cat.getCatnumber());
			pstmt.setString(2, cat.getName());
			pstmt.setDate(3, new java.sql.Date(cat.getBirthday().getTime()));
			pstmt.setString(4, cat.getSex());
			pstmt.setString(5, cat.getKind());
			pstmt.setString(6, cat.getColor());
			pstmt.setString(7, cat.getCost());
			pstmt.setDate(8, new java.sql.Date(cat.getSupplydate().getTime()));
			pstmt.setInt(9, cat.getCompanyNum());
			int r = pstmt.executeUpdate();
			return r > 0;
		} catch (SQLException e) {
			e.printStackTrace();
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		return false;
	}
	
	public boolean SqlDelSellingCat(int catnum, String passportnum) {
		String query = "delete from ParcelCat where catnum='" + catnum + "' and passportnum = '"  + passportnum + "'";
		
		try {
			Statement stmt = conn.createStatement();
			int rs = stmt.executeUpdate(query);
			return rs > 0;
		} catch (SQLException e) {
			e.printStackTrace();
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		return false;
	}
	
	
	
	public Vector<SellingCat> SqlGetAllSellingCat() {
		Vector<SellingCat> cats = new Vector<>();
		String query = "select * from ParcelCat";
		
		try {
			Statement stmt = conn.createStatement();
			ResultSet rs = stmt.executeQuery(query);
			while (rs.next()) {
			//	System.out.println(rs.getInt(1) + " " + rs.getInt(2) + " " + rs.getString(3) + " " + rs.getString(4) + " " + rs.getString(5) + " " + rs.getString(6) + " " + rs.getString(7) + " " + rs.getString(8) + " " + rs.getString(9) + " " + rs.getString(10));
				cats.add(new SellingCat(rs.getInt(1), rs.getInt(2), rs.getString(3), rs.getDate(4), rs.getString(5), rs.getString(6), rs.getString(7), rs.getDate(8), rs.getDate(9), rs.getString(10)));
			}
		} catch (SQLException e) {
			e.printStackTrace();
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		return cats;
	}
	
	public boolean SqlAddSellingCat(SellingCat scat) {
		String query = "insert into ParcelCat values (?, ?, ?, ?, ?, ?, ?, ?, ?, ?)";
		
		try {
			PreparedStatement pstmt = conn.prepareStatement(query);
			pstmt.setInt(1, scat.getCatnum());
			pstmt.setInt(2, scat.getScid());
			pstmt.setString(3, scat.getPassport());
			pstmt.setDate(4, new java.sql.Date(scat.getSellingDate().getTime()));
			pstmt.setString(5, scat.getIdnum());
			pstmt.setString(6, scat.getTransmethod());
			pstmt.setString(7, scat.getTcompany());
			pstmt.setDate(8, new java.sql.Date(scat.getTdepart().getTime()));
			pstmt.setDate(9, new java.sql.Date(scat.getTarriavl().getTime()));
			pstmt.setString(10, scat.getTprice());
			int rs = pstmt.executeUpdate();
			return rs > 0;
		} catch (SQLException e) {
			e.printStackTrace();
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		return false;
	}
	
	public boolean SqlAddSellingCat(int catnum, int id, String passport, Date parcelDate, String adopterid, String tmethod, String tcompany, Date tdepart, Date tarrival, String tprice) {
		String query = "insert into ParcelCat values (?, ?, ?, ?, ?, ?, ?, ?, ?, ?)";
		
		try {
			PreparedStatement pstmt = conn.prepareStatement(query);
			pstmt.setInt(1, catnum);
			pstmt.setInt(2, id);
			pstmt.setString(3, passport);
			pstmt.setDate(4, new java.sql.Date(parcelDate.getTime()));
			pstmt.setString(5, adopterid);
			pstmt.setString(6, tmethod);
			pstmt.setString(7, tcompany);
			pstmt.setDate(8, new java.sql.Date(tdepart.getTime()));
			pstmt.setDate(9, new java.sql.Date(tarrival.getTime()));
			pstmt.setString(10, tprice);
			int rs = pstmt.executeUpdate();
			return rs > 0;
		} catch (SQLException e) {
			e.printStackTrace();
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		return false;
	}
	
	public boolean SqlAddCatVaccin(CatVaccin catvaccin) {
		String query = "insert into Cat_Hospital_Vaccin values (?, ?, ?)";
		
		try {
			PreparedStatement pstmt = conn.prepareStatement(query);
			pstmt.setInt(1, catvaccin.getCatnum());
			pstmt.setInt(2, catvaccin.getVaccinnum());
			pstmt.setDate(3, new java.sql.Date(catvaccin.getVdate().getTime()));
			int rs = pstmt.executeUpdate();
			return rs > 0;
		} catch (SQLException e) {
			e.printStackTrace();
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		return false;
	}
	
	public boolean SqlAddCatVaccin(int catnum, int vaccinnum, Date date) {
		String query = "insert into Cat_Hospital_Vaccin values (?, ?, ?)";
		
		try {
			PreparedStatement pstmt = conn.prepareStatement(query);
			pstmt.setInt(1, catnum);
			pstmt.setInt(2, vaccinnum);
			pstmt.setDate(3, new java.sql.Date(date.getTime()));
			int rs = pstmt.executeUpdate();
			return rs > 0;
		} catch (SQLException e) {
			e.printStackTrace();
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		return false;
	}
	
	public Vector<Cattery> SqlGetAllCattery() {
		Vector<Cattery> catteries = new Vector<>();
		String query = "select * from cattery";

		try {
			Statement stmt = conn.createStatement();
			ResultSet rs = stmt.executeQuery(query);
			while (rs.next()) {
				catteries.add(new Cattery(rs.getInt(1), rs.getString(2), rs.getString(3), rs.getString(4)));
			}
		} catch (SQLException e) {
			// Logging here
			e.printStackTrace();
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		return catteries;
	}
	
	public Vector<Adopter> SqlGetAllAdopter() {
		Vector<Adopter> adopters = new Vector<>();
		String query = "select * from Adoption";

		try {
			Statement stmt = conn.createStatement();
			ResultSet rs = stmt.executeQuery(query);
			while (rs.next()) {
				adopters.add(new Adopter(rs.getString(1), rs.getString(2), rs.getString(3), rs.getString(4)));
			}
		} catch (SQLException e) {
			// Logging here
			e.printStackTrace();
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		return adopters;
	}
	
	public boolean SqlUpdateAdopterWithId(String id, String name, String addr, String call) {
		String query = "update Adoption set idname='" + name +"', idadress='" + addr + "', idcallnum='" + call +"' where idnum='" + id + "'";
		
		try {
			Statement stmt = conn.createStatement();
			int rs = stmt.executeUpdate(query);
			return rs > 0;
		} catch (SQLException e) {
			e.printStackTrace();
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		return false;
	}
	
	public boolean SqlDeleteAdopterWithId(String id) {
		String query = "delete from Adoption where idnum='" + id + "'";
		
		try {
			Statement stmt = conn.createStatement();
			int rs = stmt.executeUpdate(query);
			return rs > 0;
		} catch (SQLException e) {
			e.printStackTrace();
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		return false;
	}
	
	public Vector<Hospital> SqlGetAllHospital() {
		Vector<Hospital> vacs = new Vector<>();
		String query = "select * from hospital";
		
		try {
			Statement stmt = conn.createStatement();
			ResultSet rs = stmt.executeQuery(query);
			while (rs.next()) {
				vacs.add(new Hospital(rs.getInt(1), rs.getString(2)));
			}
		} catch (SQLException e) {
			e.printStackTrace();
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		return vacs;
	}
	
	public Vector<CatVaccin> SqlGetAllCatVaccin() {
		Vector<CatVaccin> vacs = new Vector<>();
		String query = "select * from Cat_Hospital_Vaccin";
		
		try {
			Statement stmt = conn.createStatement();
			ResultSet rs = stmt.executeQuery(query);
			while (rs.next()) {
				vacs.add(new CatVaccin(rs.getInt(1), rs.getInt(2), rs.getDate(3)));
			}
		} catch (SQLException e) {
			e.printStackTrace();
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		return vacs;
	}
	
	public Vector<Pair<String, String>> SqlGetVaccinCount() {
		Vector<Pair<String, String>> rv = new Vector<>();
		String query = "select hospital.vaccinname, count(cat_hospital_vaccin.catnum) as total from hospital, cat_hospital_vaccin where hospital.vaccinnum = cat_hospital_vaccin.vaccinnum group by hospital.vaccinname";
		
		try {
			Statement stmt = conn.createStatement();
			ResultSet rs = stmt.executeQuery(query);
			while (rs.next()) {
				rv.add(new Pair<String, String>(rs.getString(1), rs.getString(2)));
			}
		} catch (SQLException e) {
			e.printStackTrace();
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		return rv;
	}
	
	public Vector<Pair<String, String>> SqlGetAllCatOfAdopter(String rrn) {
		Vector<Pair<String, String>> resultPair = new Vector<>();
		String query = "select Cat.catnum, Cat.catname from Cat, ParcelCat, Adoption where Adoption.idnum = ParcelCat.idnum and ParcelCat.catnum = Cat.catnum and Adoption.idnum = '" + rrn + "'";
		
		try {
			Statement stmt = conn.createStatement();
			ResultSet rs = stmt.executeQuery(query);
			while (rs.next()) {
				resultPair.add(new Pair<String, String>(rs.getString(1), rs.getString(2)));
			}
		} catch (SQLException e) {
			e.printStackTrace();
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		return resultPair;
	}
	
	public Vector<Triple<String, String, String>> SqlGetAllCatOfAdopterOfVaccin(String rrn) {
		Vector<Triple<String, String, String>> resultPair = new Vector<>();
		String query = "select Cat.catnum, Hospital.vaccinname, Cat_Hospital_Vaccin.vdate from Hospital, Cat_Hospital_Vaccin, Cat, ParcelCat, Adoption where Adoption.idnum = ParcelCat.idnum and ParcelCat.catnum = Cat.catnum and Cat.catnum = cat_hospital_vaccin.catnum and cat_hospital_vaccin.vaccinnum = hospital.vaccinnum and Adoption.idnum = '" + rrn + "'";
		
		try {
			Statement stmt = conn.createStatement();
			ResultSet rs = stmt.executeQuery(query);
			while (rs.next()) {
				resultPair.add(new Triple<String, String, String>(String.valueOf(rs.getInt(1)), rs.getString(2), rs.getString(3)));
			}
		} catch (SQLException e) {
			e.printStackTrace();
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		return resultPair;
	}
	
	public Vector<Pair<String, String>> SqlGetIntersectVaccin(String[] vaccinnum) {
		Vector<Pair<String, String>> cats = new Vector<>();

		if (vaccinnum.length <= 0)
			return null;
		
		String query = "select Cat.catnum, Cat.catname from Cat where Cat.catnum in ((select catnum from Cat_Hospital_Vaccin where vaccinnum = " + vaccinnum[0] + ")";
		for (int i=1; i<vaccinnum.length; ++i) {
			query += " intersect ";
			query += "(select catnum from Cat_Hospital_Vaccin where vaccinnum = " + vaccinnum[i] + ")";
		}
		query += ")";
		
		try {
			Statement stmt = conn.createStatement();
			ResultSet rs = stmt.executeQuery(query);
			while (rs.next()) {
				cats.add(new Pair<String, String>(rs.getString(1), rs.getString(2)));
			}
		} catch (SQLException e) {
			e.printStackTrace();
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		return cats;
	}
	
	public Vector<Cattery> SqlGetSearchCattery(String attr, String q) {
		Vector<Cattery> catteries = new Vector<>();
		String query = "select * from cattery where " + attr + " like '%" + q + "%'";
		
		System.out.println("Query = " + query);
		
		try {
			Statement stmt = conn.createStatement();
			ResultSet rs = stmt.executeQuery(query);
			while (rs.next()) {
				catteries.add(new Cattery(rs.getInt(1), rs.getString(2), rs.getString(3), rs.getString(4)));
			}
		} catch (SQLException e) {
			e.printStackTrace();
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		return catteries;
	}
	
	public Vector<Pair<String, String>> SqlGetSellingDateOfCattery(String companynum) {
		Vector<Pair<String, String>> v = new Vector<>();
		String query = "select ParcelCat.catnum, ParcelCat.parceloutdate from ParcelCat where ParcelCat.catnum in ("
				+ "select Cat.catnum from Cat where Cat.company in ("
				+ "select cattery.company from cattery where cattery.company = '" + companynum + "'))";
		
		try {
			Statement stmt = conn.createStatement();
			ResultSet rs = stmt.executeQuery(query);
			while (rs.next()) {
				v.add(new Pair<String, String>(rs.getString(1), rs.getString(2)));
			}
		} catch (SQLException e) {
			e.printStackTrace();
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		return v;		
	}
	
	public boolean SqlAddCattery(Cattery cattery) {
		String query = "insert into cattery values (" + cattery.getId() + ", '" + cattery.getName() + "', '" + cattery.getAddress() + "', '" + cattery.getCallnumber() + "')";

		try {
			Statement stmt = conn.createStatement();
			int rCount = stmt.executeUpdate(query);
			return (rCount > 0);
		} catch (SQLException e) {
			e.printStackTrace();
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		return false;
	}
	
	public boolean SqlAddAdopter(Adopter adopter) {
		String query = "insert into Adoption values (" + adopter.getRrnumber() + ", '" + adopter.getName() + "', '" + adopter.getAddress() + "', '" + adopter.getCallnumber() + "')";
		
		try {
			Statement stmt = conn.createStatement();
			int rCount = stmt.executeUpdate(query);
			return (rCount > 0);
		} catch (SQLException e) {
			e.printStackTrace();
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		return false;
	}
	
	public static DbManager getInstance() {
		return instance;
	}

}
