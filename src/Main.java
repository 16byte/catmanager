import java.awt.BorderLayout;
import java.awt.EventQueue;

import Database.DbManager;
import View.MainView;

public class Main {
	public Main() {
		new MainView();
		
		DbManager.getInstance().Finalize();
	}

}
