package View;

import Model.*;
import javax.swing.JPanel;
import javax.swing.JLabel;
import javax.swing.JTextField;
import javax.swing.JTable;
import javax.swing.ListSelectionModel;
import javax.swing.table.DefaultTableModel;

import java.util.Vector;

import javax.swing.JButton;
import javax.swing.JScrollPane;
import javax.swing.JTabbedPane;

public class AdopterPanel extends JPanel {
	
	private AdopterModel model;
	private JTextField idTxtField;
	private JTextField nameTxtField;
	private JTextField addrTxtField;
	private JTextField callTxtField;
	private JTable adopterTable;
	private JButton addButton;
	
	private Vector<String> tableColumn = new Vector<>();
	private DefaultTableModel tableModel;
	private JScrollPane resultPanel;
	private JTable resultTable;
	private JTextField rrnTxtField;
	
	private Vector<String> rsTableColumn = new Vector<>();
	private DefaultTableModel rsTableModel;
	private JButton rrnButton;
	
	private Vector<String> vrsTableColumn = new Vector<>();
	private DefaultTableModel vrsTableModel;
	private JTextField vaccinTextField;
	private JButton vaccinButton;
	private JScrollPane vresultPanel;
	private JTable vresultTable;
	private JButton delButton;
	private JButton ModifyButton;
	
	/**
	 * Create the panel.
	 */
	public AdopterPanel(AdopterModel m) {
		this.model = m;
		setLayout(null);
		
		JLabel idLabel = new JLabel("Id");
		idLabel.setBounds(25, 22, 105, 18);
		add(idLabel);
		
		idTxtField = new JTextField();
		idTxtField.setBounds(14, 51, 116, 24);
		add(idTxtField);
		idTxtField.setColumns(10);
		
		JLabel nameLabel = new JLabel("Name");
		nameLabel.setBounds(25, 99, 105, 18);
		add(nameLabel);
		
		nameTxtField = new JTextField();
		nameTxtField.setBounds(14, 129, 116, 24);
		add(nameTxtField);
		nameTxtField.setColumns(10);
		
		JLabel addrLabel = new JLabel("Address");
		addrLabel.setBounds(25, 175, 105, 18);
		add(addrLabel);
		
		addrTxtField = new JTextField();
		addrTxtField.setBounds(14, 214, 116, 24);
		add(addrTxtField);
		addrTxtField.setColumns(10);
		
		JLabel callLabel = new JLabel("Call Number");
		callLabel.setBounds(25, 261, 105, 18);
		add(callLabel);
		
		callTxtField = new JTextField();
		callTxtField.setBounds(14, 300, 116, 24);
		add(callTxtField);
		callTxtField.setColumns(10);
		
		tableColumn.addElement("Id");
		tableColumn.addElement("Name");
		tableColumn.addElement("Address");
		tableColumn.addElement("Call");
		
		tableModel = new DefaultTableModel(tableColumn, 0) {
			@Override
			public boolean isCellEditable(int row, int column) {
				// TODO Auto-generated method stub
				return false;
			}
		};
		
		addButton = new JButton("Add");
		addButton.setBounds(177, 299, 75, 27);
		add(addButton);
		
		rsTableColumn.addElement("CatNum");
		rsTableColumn.addElement("CatName");
		
		rsTableModel = new DefaultTableModel(rsTableColumn, 0) {
			@Override
			public boolean isCellEditable(int row, int column) {
				// TODO Auto-generated method stub
				return false;
			}
		};
		
		JTabbedPane tabbedPane = new JTabbedPane(JTabbedPane.TOP);
		tabbedPane.setBounds(469, 12, 325, 376);
		add(tabbedPane);
		
		JPanel CatPanel = new JPanel();
		tabbedPane.addTab("\uACE0\uC591\uC774", null, CatPanel, null);
		CatPanel.setLayout(null);
		
		rrnTxtField = new JTextField();
		rrnTxtField.setBounds(14, 12, 116, 24);
		CatPanel.add(rrnTxtField);
		rrnTxtField.setColumns(10);
		
		rrnButton = new JButton("\uC870\uD68C");
		rrnButton.setBounds(245, 11, 61, 27);
		CatPanel.add(rrnButton);
		
		resultPanel = new JScrollPane();
		resultPanel.setBounds(0, 48, 317, 284);
		CatPanel.add(resultPanel);
				
		resultTable = new JTable(rsTableModel);
		resultPanel.setViewportView(resultTable);
		
		JPanel VaccinPannel = new JPanel();
		tabbedPane.addTab("\uC811\uC885", null, VaccinPannel, null);
		VaccinPannel.setLayout(null);
		
		vaccinTextField = new JTextField();
		vaccinTextField.setBounds(14, 12, 116, 24);
		VaccinPannel.add(vaccinTextField);
		vaccinTextField.setColumns(10);
		
		vaccinButton = new JButton("\uC870\uD68C");
		vaccinButton.setBounds(201, 11, 105, 27);
		VaccinPannel.add(vaccinButton);
		
		vresultPanel = new JScrollPane();
		vresultPanel.setBounds(14, 47, 292, 297);
		VaccinPannel.add(vresultPanel);
		
		vrsTableColumn.addElement("CatNum");
		vrsTableColumn.addElement("VaccinName");
		vrsTableColumn.addElement("VaccinDate");
		vrsTableModel = new DefaultTableModel(vrsTableColumn, 0);
		
		vresultTable = new JTable(vrsTableModel);
		vresultPanel.setViewportView(vresultTable);
		
		JScrollPane scrollPane = new JScrollPane();
		scrollPane.setBounds(177, 22, 278, 265);
		add(scrollPane);
		
		adopterTable = new JTable(tableModel);
		scrollPane.setViewportView(adopterTable);
		adopterTable.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
		
		delButton = new JButton("Del");
		delButton.setBounds(380, 299, 75, 27);
		add(delButton);
		
		ModifyButton = new JButton("Modify");
		ModifyButton.setBounds(266, 299, 89, 27);
		add(ModifyButton);
	}

	public JButton getModifyButton() {
		return ModifyButton;
	}

	public void setModifyButton(JButton modifyButton) {
		ModifyButton = modifyButton;
	}

	public JButton getDelButton() {
		return delButton;
	}

	public void setDelButton(JButton delButton) {
		this.delButton = delButton;
	}

	public JTextField getAddrTxtField() {
		return addrTxtField;
	}

	public void setAddrTxtField(JTextField addrTxtField) {
		this.addrTxtField = addrTxtField;
	}

	public JTextField getCallTxtField() {
		return callTxtField;
	}

	public void setCallTxtField(JTextField callTxtField) {
		this.callTxtField = callTxtField;
	}

	public JButton getAddButton() {
		return addButton;
	}

	public void setAddButton(JButton addButton) {
		this.addButton = addButton;
	}

	public JTextField getIdTxtField() {
		return idTxtField;
	}

	public void setIdTxtField(JTextField idTxtField) {
		this.idTxtField = idTxtField;
	}

	public JTextField getNameTxtField() {
		return nameTxtField;
	}

	public void setNameTxtField(JTextField nameTxtField) {
		this.nameTxtField = nameTxtField;
	}

	public JTable getAdopterTable() {
		return adopterTable;
	}

	public void setAdopterTable(JTable adopterTable) {
		this.adopterTable = adopterTable;
	}

	public DefaultTableModel getTableModel() {
		return tableModel;
	}

	public void setTableModel(DefaultTableModel tableModel) {
		this.tableModel = tableModel;
	}

	public JTextField getRrnField() {
		return rrnTxtField;
	}

	public void setRrnField(JTextField rrnField) {
		this.rrnTxtField = rrnField;
	}

	public DefaultTableModel getRsTableModel() {
		return rsTableModel;
	}

	public void setRsTableModel(DefaultTableModel rsTableModel) {
		this.rsTableModel = rsTableModel;
	}

	public JTextField getRrnTxtField() {
		return rrnTxtField;
	}

	public void setRrnTxtField(JTextField rrnTxtField) {
		this.rrnTxtField = rrnTxtField;
	}

	public JButton getRrnButton() {
		return rrnButton;
	}

	public void setRrnButton(JButton rrnButton) {
		this.rrnButton = rrnButton;
	}

	public DefaultTableModel getVrsTableModel() {
		return vrsTableModel;
	}

	public void setVrsTableModel(DefaultTableModel vrsTableModel) {
		this.vrsTableModel = vrsTableModel;
	}

	public JTextField getVaccinTextField() {
		return vaccinTextField;
	}

	public void setVaccinTextField(JTextField vaccinTextField) {
		this.vaccinTextField = vaccinTextField;
	}

	public JButton getVaccinButton() {
		return vaccinButton;
	}

	public void setVaccinButton(JButton vaccinButton) {
		this.vaccinButton = vaccinButton;
	}
}
