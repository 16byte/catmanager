package View;

import javax.swing.JPanel;
import javax.swing.JScrollPane;

import java.util.Vector;

import javax.swing.JButton;
import javax.swing.JTextField;
import javax.swing.table.DefaultTableModel;

import Model.HospitalModel;

import javax.swing.JLabel;
import javax.swing.JTable;

public class HospitalView extends JPanel {
	private HospitalModel model;
	
	private JTextField vnumTxtField;
	private JTextField vnameTxtField;
	private JTable vTable;

	private Vector<String> tableColumn = new Vector<String>();
	private DefaultTableModel tableModel;
	private JTable resultTable;
	
	private Vector<String> rtableColumn = new Vector<>();
	private DefaultTableModel rtableModel;
	private JButton updateButton;
	
	/**
	 * Create the panel.
	 */
	public HospitalView(HospitalModel m) {
		model = m;
		
		setLayout(null);
		
		JScrollPane vaccinScrollPanel = new JScrollPane();
		vaccinScrollPanel.setBounds(203, 12, 213, 308);
		add(vaccinScrollPanel);
		
		tableColumn.addElement("Vaccin Number");
		tableColumn.addElement("Vaccin Name");
		tableModel = new DefaultTableModel(tableColumn, 0);
		
		vTable = new JTable(tableModel);
		vaccinScrollPanel.setViewportView(vTable);
		
		JLabel vnumLabel = new JLabel("Vaccin Number");
		vnumLabel.setBounds(42, 23, 116, 18);
		add(vnumLabel);
		
		vnumTxtField = new JTextField();
		vnumTxtField.setBounds(42, 52, 116, 24);
		add(vnumTxtField);
		vnumTxtField.setColumns(10);
		
		JLabel vnameLabel = new JLabel("Vaccine Name");
		vnameLabel.setBounds(42, 130, 116, 18);
		add(vnameLabel);
		
		vnameTxtField = new JTextField();
		vnameTxtField.setBounds(42, 160, 116, 24);
		add(vnameTxtField);
		vnameTxtField.setColumns(10);
		
		JButton addButton = new JButton("Add");
		addButton.setBounds(200, 344, 105, 27);
		add(addButton);
		
		rtableColumn.addElement("백신명");
		rtableColumn.addElement("마리수");
		rtableModel = new DefaultTableModel(rtableColumn, 0) {
			@Override
			public boolean isCellEditable(int row, int column) {
				// TODO Auto-generated method stub
				return false;
			}
		};
		
		resultTable = new JTable(rtableModel);
		resultTable.setBounds(483, 57, 221, 308);
		add(resultTable);
		
		updateButton = new JButton("Update");
		updateButton.setBounds(525, 19, 105, 27);
		add(updateButton);
		
	}

	public JTable getvTable() {
		return vTable;
	}

	public void setvTable(JTable vTable) {
		this.vTable = vTable;
	}

	public JButton getUpdateButton() {
		return updateButton;
	}

	public void setUpdateButton(JButton updateButton) {
		this.updateButton = updateButton;
	}

	public DefaultTableModel getRtableModel() {
		return rtableModel;
	}

	public void setRtableModel(DefaultTableModel rtableModel) {
		this.rtableModel = rtableModel;
	}

	public JTextField getVnumTxtField() {
		return vnumTxtField;
	}

	public void setVnumTxtField(JTextField vnumTxtField) {
		this.vnumTxtField = vnumTxtField;
	}

	public JTextField getVnameTxtField() {
		return vnameTxtField;
	}

	public void setVnameTxtField(JTextField vnameTxtField) {
		this.vnameTxtField = vnameTxtField;
	}

	public DefaultTableModel getTableModel() {
		return tableModel;
	}

	public void setTableModel(DefaultTableModel tableModel) {
		this.tableModel = tableModel;
	}
}
