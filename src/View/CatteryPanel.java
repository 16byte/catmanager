package View;

import java.awt.event.ComponentEvent;
import java.awt.event.ComponentListener;
import java.beans.PropertyChangeEvent;
import java.util.Vector;
import java.util.function.Consumer;

import javax.swing.JOptionPane;
import javax.swing.JPanel;
import Controller.*;
import Database.DbManager;
import Model.*;
import View.*;
import javax.swing.JTable;
import javax.swing.event.ListSelectionListener;
import javax.swing.event.TableColumnModelListener;
import javax.swing.event.TableModelEvent;
import javax.swing.event.TableModelListener;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableModel;

import java.awt.Panel;
import java.awt.Label;
import java.awt.TextField;
import java.awt.Button;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import javax.swing.JButton;
import javax.swing.JTextField;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.ListSelectionModel;
import javax.swing.AbstractListModel;
import javax.swing.JComboBox;
import javax.swing.DefaultComboBoxModel;
import javax.swing.DefaultListSelectionModel;
import javax.swing.UIManager;
import javax.swing.border.TitledBorder;
import javax.swing.JScrollPane;
import javax.swing.JTabbedPane;

public class CatteryPanel extends AbstractViewPanel {
	private CatteryModel model;
	
	private Vector<String> tableColumn = new Vector<String>();
	private DefaultTableModel tableModel;
	private TextField callTxtField;
	private Label callLabel;
	private TextField addrTxtField;
	private Label addrLabel;
	private TextField nameTxtField;
	private Label nameLabel;
	private TextField companyTxtField;
	private Label companyLabel;
	private Panel inputPanel;
	private Button addButton;
	private JTable catteryTable;
	private DefaultTableModel searchTableModel;
	private JTable searchTable;
	private JTextField SearchTxtField;
	private JButton searchButton;
	private JLabel searchResultLabel;
	private JComboBox searchComboBox;
	private JScrollPane scrollPane;
	private JTextField dateTxtField;
	private Vector<String> ctableColumn = new Vector<String>();
	private DefaultTableModel ctableModel;
	private JTable dateTable;
	private JButton dateButton;
	
	/**
	 * Create the panel.
	 */
	public CatteryPanel(CatteryModel catteryModel) {
		this.model = catteryModel;
		setLayout(null);
		
		tableColumn.addElement("Company");
		tableColumn.addElement("Name");
		tableColumn.addElement("Address");
		tableColumn.addElement("CallNum");
		
		tableModel = new DefaultTableModel(tableColumn, 0) {
			@Override
			public boolean isCellEditable(int row, int column) {
                return false;
			}
		};
		
		JScrollPane catteryScrollPane = new JScrollPane();
		catteryScrollPane.setBounds(196, 12, 345, 276);
		add(catteryScrollPane);
		catteryTable = new JTable(tableModel);
		catteryTable.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
		catteryScrollPane.setViewportView(catteryTable);
		
		inputPanel = new Panel();
		inputPanel.setBounds(10, 12, 177, 276);
		add(inputPanel);
		inputPanel.setLayout(null);
		
		companyLabel = new Label("\uC5C5\uCCB4\uBC88\uD638");
		companyLabel.setBounds(30, 0, 113, 25);
		inputPanel.add(companyLabel);
		
		companyTxtField = new TextField();
		companyTxtField.setBounds(30, 30, 113, 25);
		inputPanel.add(companyTxtField);
		
		nameLabel = new Label("Name");
		nameLabel.setBounds(30, 66, 113, 25);
		inputPanel.add(nameLabel);
		
		nameTxtField = new TextField();
		nameTxtField.setBounds(33, 97, 110, 25);
		inputPanel.add(nameTxtField);
		
		addrLabel = new Label("Address");
		addrLabel.setBounds(30, 128, 113, 25);
		inputPanel.add(addrLabel);
		
		addrTxtField = new TextField();
		addrTxtField.setBounds(0, 159, 167, 25);
		inputPanel.add(addrTxtField);
		
		callLabel = new Label("Call Number");
		callLabel.setBounds(30, 198, 113, 25);
		inputPanel.add(callLabel);
		
		callTxtField = new TextField();
		callTxtField.setBounds(30, 229, 113, 25);
		inputPanel.add(callTxtField);
		
		addButton = new Button("Add");
		
		addButton.setBounds(218, 294, 95, 41);
		add(addButton);
		
		searchTableModel = new DefaultTableModel(tableColumn, 0);
		
		JTabbedPane tabbedPane = new JTabbedPane(JTabbedPane.TOP);
		tabbedPane.setBounds(555, 5, 320, 398);
		add(tabbedPane);
		
		JPanel searchPanel = new JPanel();
		tabbedPane.addTab("Search", null, searchPanel, null);
		searchPanel.setLayout(null);
		searchTable = new JTable(searchTableModel);
		searchTable.setBounds(15, 72, 294, 294);
		searchPanel.add(searchTable);
		
		searchComboBox = new JComboBox();
		searchComboBox.setBounds(15, 6, 89, 24);
		searchPanel.add(searchComboBox);
		searchComboBox.setMaximumRowCount(4);
		searchComboBox.setModel(new DefaultComboBoxModel(new String[] {"company", "name", "address", "num"}));
		searchComboBox.setSelectedIndex(0);
		
		searchButton = new JButton("Search");
		searchButton.setBounds(109, 5, 79, 27);
		searchPanel.add(searchButton);
		
		SearchTxtField = new JTextField();
		SearchTxtField.setBounds(193, 6, 116, 24);
		searchPanel.add(SearchTxtField);
		SearchTxtField.setColumns(10);
		
		searchResultLabel = new JLabel("");
		searchResultLabel.setBounds(14, 40, 295, 18);
		searchPanel.add(searchResultLabel);
		
		JPanel companyPanel = new JPanel();
		tabbedPane.addTab("Cattery", null, companyPanel, null);
		companyPanel.setLayout(null);
		
		dateTxtField = new JTextField();
		dateTxtField.setBounds(14, 0, 116, 24);
		companyPanel.add(dateTxtField);
		dateTxtField.setColumns(10);
		
		dateButton = new JButton("\uC870\uD68C");
		dateButton.setBounds(196, -1, 105, 27);
		companyPanel.add(dateButton);
		
		ctableColumn.addElement("고양이번호");
		ctableColumn.addElement("분양날짜");
		ctableModel = new DefaultTableModel(ctableColumn, 0);
		
		dateTable = new JTable(ctableModel);
		dateTable.setBounds(14, 48, 287, 306);
		companyPanel.add(dateTable);
	}

	public JButton getDateButton() {
		return dateButton;
	}

	public void setDateButton(JButton dateButton) {
		this.dateButton = dateButton;
	}

	public JTextField getSearchTxtField() {
		return SearchTxtField;
	}

	public void setSearchTxtField(JTextField searchTxtField) {
		SearchTxtField = searchTxtField;
	}

	public JButton getSearchButton() {
		return searchButton;
	}

	public void setSearchButton(JButton searchButton) {
		this.searchButton = searchButton;
	}

	public JLabel getSearchResultLabel() {
		return searchResultLabel;
	}

	public void setSearchResultLabel(JLabel searchResultLabel) {
		this.searchResultLabel = searchResultLabel;
	}

	public DefaultTableModel getTableModel() {
		return tableModel;
	}

	public void setTableModel(DefaultTableModel tableModel) {
		this.tableModel = tableModel;
	}

	public TextField getCallTxtField() {
		return callTxtField;
	}

	public void setCallTxtField(TextField callTxtField) {
		this.callTxtField = callTxtField;
	}

	public TextField getAddrTxtField() {
		return addrTxtField;
	}

	public void setAddrTxtField(TextField addrTxtField) {
		this.addrTxtField = addrTxtField;
	}

	public TextField getNameTxtField() {
		return nameTxtField;
	}

	public void setNameTxtField(TextField nameTxtField) {
		this.nameTxtField = nameTxtField;
	}

	public TextField getCompanyTxtField() {
		return companyTxtField;
	}

	public void setCompanyTxtField(TextField companyTxtField) {
		this.companyTxtField = companyTxtField;
	}

	public Button getAddButton() {
		return addButton;
	}

	public void setAddButton(Button addButton) {
		this.addButton = addButton;
	}

	public JTable getCatteryTable() {
		return catteryTable;
	}

	public void setCatteryTable(JTable catteryTable) {
		this.catteryTable = catteryTable;
	}
	
	public DefaultTableModel getSearchTableModel() {
		return searchTableModel;
	}

	public void setSearchTableModel(DefaultTableModel searchTableModel) {
		this.searchTableModel = searchTableModel;
	}
	
	public JTable getSearchTable() {
		return searchTable;
	}

	public void setSearchTable(JTable searchTable) {
		this.searchTable = searchTable;
	}
	
	public JComboBox getSearchComboBox() {
		return searchComboBox;
	}

	public void setSearchComboBox(JComboBox searchComboBox) {
		this.searchComboBox = searchComboBox;
	}

	public JTextField getDateTxtField() {
		return dateTxtField;
	}

	public void setDateTxtField(JTextField dateTxtField) {
		this.dateTxtField = dateTxtField;
	}

	public DefaultTableModel getCtableModel() {
		return ctableModel;
	}

	public void setCtableModel(DefaultTableModel ctableModel) {
		this.ctableModel = ctableModel;
	}

	@Override
	public void modelPropertyChange(PropertyChangeEvent evt) {
		// TODO Auto-generated method stub
		if (evt.getPropertyName().equals("ID")) {
			int newIdValue = (int)evt.getNewValue();
			
			tableModel.getColumnClass(0);
		}
	}
}
