package View;

import javax.swing.JPanel;

import Controller.*;
import Model.*;
import View.*;
import javax.swing.JList;
import javax.swing.JScrollPane;

import java.beans.PropertyChangeEvent;
import java.text.SimpleDateFormat;

import javax.swing.ComboBoxModel;
import javax.swing.DefaultComboBoxModel;
import javax.swing.DefaultListModel;
import javax.swing.JButton;
import javax.swing.ListSelectionModel;
import javax.swing.JLabel;
import javax.swing.JTextField;
import javax.swing.JComboBox;
import javax.swing.JFormattedTextField;

public class CatPanel extends AbstractViewPanel {
	private CatModel model;
	private SellingCatModel sellingcatModel;
	private JButton Parcelbutton;
	private JButton Deletebutton;
	private JList catList;
	
	private DefaultListModel listModel = new DefaultListModel<>(); 
	private DefaultListModel scatlistModel = new DefaultListModel<>();
	private DefaultComboBoxModel<String> boxModel = new DefaultComboBoxModel<>();
	private DefaultComboBoxModel<String> adopterBoxModel = new DefaultComboBoxModel<>();
	private DefaultComboBoxModel<String> vaccineBoxModel = new DefaultComboBoxModel<>();
	private JScrollPane catListPanel;
	private JScrollPane sellingCatPanel;
	private JTextField textField_catnum;
	private JTextField textField_name;
	private JFormattedTextField textField_Date;
	private JTextField textField_gender;
	private JTextField textField_species;
	private JTextField textField_color;
	private JTextField textField_price;
	private JTextField textField_supplydate;
	private JButton addButton;
	private JComboBox companyComboBox;
	private JLabel lblNewLabel_1;
	private JTextField textField_id;
	private JLabel lblNewLabel_2;
	private JTextField textField_passport;
	private JLabel lblNewLabel_9;
	private JTextField textField_parceldate;
	private JLabel lblNewLabel_10;
	private JTextField textField_method;
	private JLabel lblNewLabel_11;
	private JLabel lblNewLabel_parcelcom;
	private JTextField textField_tcomapny;
	private JTextField textField_tdepart;
	private JLabel lblNewLabel_13;
	private JComboBox comboBoxAdoptor;
	private JLabel lblNewLabel_14;
	private JLabel lblNewLabel_15;
	private JTextField textField_tarrival;
	private JLabel lblNewLabel_16;
	private JTextField textField_tprice;
	private JList sellingCatList;
	
	/**
	 * Create the panel.
	 */
	public CatPanel(CatModel m, SellingCatModel sm) {
		model = m;
		sellingcatModel = sm;
		
		setLayout(null);
		
		Parcelbutton = new JButton(">>");
		Parcelbutton.setBounds(476, 29, 53, 27);
		add(Parcelbutton);
		
		Deletebutton = new JButton("<<");
		Deletebutton.setBounds(476, 123, 53, 27);
		add(Deletebutton);
		
		catListPanel = new JScrollPane();
		catListPanel.setBounds(303, 31, 118, 146);
		add(catListPanel);
		
		catList = new JList(listModel);
		catListPanel.setViewportView(catList);
		catList.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
		
		sellingCatPanel = new JScrollPane();
		sellingCatPanel.setBounds(598, 11, 118, 148);
		add(sellingCatPanel);
		
		sellingCatList = new JList(scatlistModel);
		sellingCatPanel.setViewportView(sellingCatList);
		sellingCatList.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
		
		JLabel lblCatNum = new JLabel("Cat Num");
		lblCatNum.setBounds(52, 9, 62, 18);
		add(lblCatNum);
		
		textField_catnum = new JTextField();
		textField_catnum.setBounds(24, 30, 116, 24);
		add(textField_catnum);
		textField_catnum.setColumns(10);
		
		JLabel lblName = new JLabel("Name");
		lblName.setBounds(52, 67, 62, 18);
		add(lblName);
		
		textField_name = new JTextField();
		textField_name.setBounds(24, 97, 116, 24);
		add(textField_name);
		textField_name.setColumns(10);
		
		JLabel lblNewLabel_3 = new JLabel("Birthday");
		lblNewLabel_3.setBounds(52, 127, 62, 18);
		add(lblNewLabel_3);
		
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
		textField_Date = new JFormattedTextField(sdf);
		textField_Date.setBounds(24, 153, 116, 24);
		add(textField_Date);
		textField_Date.setColumns(10);
		
		JLabel lblNewLabel_4 = new JLabel("Gender");
		lblNewLabel_4.setBounds(52, 186, 62, 18);
		add(lblNewLabel_4);
		
		textField_gender = new JTextField();
		textField_gender.setBounds(24, 207, 116, 24);
		add(textField_gender);
		textField_gender.setColumns(10);
		
		JLabel lblNewLabel_5 = new JLabel("Species");
		lblNewLabel_5.setBounds(52, 234, 62, 18);
		add(lblNewLabel_5);
		
		textField_species = new JTextField();
		textField_species.setBounds(24, 264, 116, 24);
		add(textField_species);
		textField_species.setColumns(10);
		
		JLabel lblNewLabel_6 = new JLabel("Color");
		lblNewLabel_6.setBounds(189, 13, 62, 18);
		add(lblNewLabel_6);
		
		textField_color = new JTextField();
		textField_color.setBounds(154, 30, 116, 24);
		add(textField_color);
		textField_color.setColumns(10);
		
		JLabel lblNewLabel_7 = new JLabel("Price");
		lblNewLabel_7.setBounds(189, 67, 62, 18);
		add(lblNewLabel_7);
		
		textField_price = new JTextField();
		textField_price.setBounds(154, 97, 116, 24);
		add(textField_price);
		textField_price.setColumns(10);
		
		JLabel lblNewLabel_8 = new JLabel("Supply Date");
		lblNewLabel_8.setBounds(154, 127, 116, 18);
		add(lblNewLabel_8);
		
		textField_supplydate = new JTextField();
		textField_supplydate.setBounds(154, 153, 116, 24);
		add(textField_supplydate);
		textField_supplydate.setColumns(10);


		companyComboBox = new JComboBox();
		companyComboBox.setBounds(154, 207, 116, 24);
		companyComboBox.setModel(boxModel);
		add(companyComboBox);
		
		addButton = new JButton("Add");
		addButton.setBounds(24, 310, 105, 27);
		add(addButton);
		
		lblNewLabel_1 = new JLabel("\uC2DD\uBCC4\uBC88\uD638");
		lblNewLabel_1.setBounds(323, 210, 62, 18);
		add(lblNewLabel_1);
		
		textField_id = new JTextField();
		textField_id.setBounds(305, 231, 116, 24);
		add(textField_id);
		textField_id.setColumns(10);
		
		lblNewLabel_2 = new JLabel("\uC5EC\uAD8C\uBC88\uD638");
		lblNewLabel_2.setBounds(323, 267, 62, 18);
		add(lblNewLabel_2);
		
		textField_passport = new JTextField();
		textField_passport.setBounds(303, 288, 116, 24);
		add(textField_passport);
		textField_passport.setColumns(10);
		
		lblNewLabel_9 = new JLabel("\uBD84\uC591\uB420\uB0A0\uC9DC");
		lblNewLabel_9.setBounds(313, 319, 108, 18);
		add(lblNewLabel_9);
		
		textField_parceldate = new JTextField();
		textField_parceldate.setEditable(false);
		textField_parceldate.setBounds(305, 344, 116, 24);
		add(textField_parceldate);
		textField_parceldate.setColumns(10);
		
		lblNewLabel_10 = new JLabel("\uC785\uC591\uC790");
		lblNewLabel_10.setBounds(467, 210, 62, 18);
		add(lblNewLabel_10);
		
		textField_method = new JTextField();
		textField_method.setBounds(433, 288, 116, 24);
		add(textField_method);
		textField_method.setColumns(10);
		
		lblNewLabel_11 = new JLabel("\uBC30\uC1A1\uC218\uB2E8");
		lblNewLabel_11.setBounds(467, 267, 62, 18);
		add(lblNewLabel_11);
		
		lblNewLabel_parcelcom = new JLabel("\uBC30\uC1A1\uC5C5\uCCB4");
		lblNewLabel_parcelcom.setBounds(467, 314, 62, 18);
		add(lblNewLabel_parcelcom);
		
		textField_tcomapny = new JTextField();
		textField_tcomapny.setBounds(435, 344, 116, 24);
		add(textField_tcomapny);
		textField_tcomapny.setColumns(10);
		
		textField_tdepart = new JTextField();
		textField_tdepart.setBounds(613, 231, 116, 24);
		add(textField_tdepart);
		textField_tdepart.setColumns(10);
		
		lblNewLabel_13 = new JLabel("\uC5C5\uCCB4 \uBC88\uD638");
		lblNewLabel_13.setBounds(178, 186, 62, 18);
		add(lblNewLabel_13);
		
		comboBoxAdoptor = new JComboBox();
		comboBoxAdoptor.setBounds(435, 231, 164, 24);
		comboBoxAdoptor.setModel(adopterBoxModel);
		add(comboBoxAdoptor);
		
		lblNewLabel_14 = new JLabel("\uBC30\uC1A1\uCD9C\uBC1C\uB0A0\uC9DC");
		lblNewLabel_14.setBounds(641, 210, 116, 18);
		add(lblNewLabel_14);
		
		lblNewLabel_15 = new JLabel("\uBC30\uC1A1\uB3C4\uCC29\uB0A0\uC9DC");
		lblNewLabel_15.setBounds(641, 267, 116, 18);
		add(lblNewLabel_15);
		
		textField_tarrival = new JTextField();
		textField_tarrival.setBounds(613, 288, 116, 24);
		add(textField_tarrival);
		textField_tarrival.setColumns(10);
		
		lblNewLabel_16 = new JLabel("\uBC30\uC1A1\uBE44");
		lblNewLabel_16.setBounds(641, 314, 116, 18);
		add(lblNewLabel_16);
		
		textField_tprice = new JTextField();
		textField_tprice.setBounds(613, 344, 116, 24);
		add(textField_tprice);
		textField_tprice.setColumns(10);
	}
	
	public JButton getParcelbutton() {
		return Parcelbutton;
	}

	public void setParcelbutton(JButton parcelbutton) {
		Parcelbutton = parcelbutton;
	}

	public JButton getDeletebutton() {
		return Deletebutton;
	}

	public void setDeletebutton(JButton deletebutton) {
		Deletebutton = deletebutton;
	}

	public DefaultComboBoxModel<String> getVaccineBoxModel() {
		return vaccineBoxModel;
	}

	public void setVaccineBoxModel(DefaultComboBoxModel<String> vaccineBoxModel) {
		this.vaccineBoxModel = vaccineBoxModel;
	}

	public DefaultComboBoxModel<String> getAdopterBoxModel() {
		return adopterBoxModel;
	}

	public void setAdopterBoxModel(DefaultComboBoxModel<String> adopterBoxModel) {
		this.adopterBoxModel = adopterBoxModel;
	}

	public JComboBox getComboBoxAdoptor() {
		return comboBoxAdoptor;
	}

	public void setComboBoxAdoptor(JComboBox comboBoxAdoptor) {
		this.comboBoxAdoptor = comboBoxAdoptor;
	}

	public SellingCatModel getSellingcatModel() {
		return sellingcatModel;
	}

	public void setSellingcatModel(SellingCatModel sellingcatModel) {
		this.sellingcatModel = sellingcatModel;
	}

	public JTextField getTextField_id() {
		return textField_id;
	}

	public void setTextField_id(JTextField textField_id) {
		this.textField_id = textField_id;
	}

	public JTextField getTextField_passport() {
		return textField_passport;
	}

	public void setTextField_passport(JTextField textField_passport) {
		this.textField_passport = textField_passport;
	}

	public JTextField getTextField_parceldate() {
		return textField_parceldate;
	}

	public void setTextField_parceldate(JTextField textField_parceldate) {
		this.textField_parceldate = textField_parceldate;
	}

	public JTextField getTextField_method() {
		return textField_method;
	}

	public void setTextField_method(JTextField textField_method) {
		this.textField_method = textField_method;
	}

	public JTextField getTextField_tcomapny() {
		return textField_tcomapny;
	}

	public void setTextField_tcomapny(JTextField textField_tcomapny) {
		this.textField_tcomapny = textField_tcomapny;
	}

	public JTextField getTextField_tdepart() {
		return textField_tdepart;
	}

	public void setTextField_tdepart(JTextField textField_tdepart) {
		this.textField_tdepart = textField_tdepart;
	}

	public JTextField getTextField_tarrival() {
		return textField_tarrival;
	}

	public void setTextField_tarrival(JTextField textField_tarrival) {
		this.textField_tarrival = textField_tarrival;
	}

	public JTextField getTextField_tprice() {
		return textField_tprice;
	}

	public void setTextField_tprice(JTextField textField_tprice) {
		this.textField_tprice = textField_tprice;
	}

	public JList getSellingCatList() {
		return sellingCatList;
	}

	public void setSellingCatList(JList sellingCatList) {
		this.sellingCatList = sellingCatList;
	}

	public JComboBox getCompanyComboBox() {
		return companyComboBox;
	}

	public void setCompanyComboBox(JComboBox companyComboBox) {
		this.companyComboBox = companyComboBox;
	}

	public JButton getAddButton() {
		return addButton;
	}

	public void setAddButton(JButton addButton) {
		this.addButton = addButton;
	}

	public DefaultComboBoxModel<String> getBoxModel() {
		return boxModel;
	}

	public void setBoxModel(DefaultComboBoxModel<String> boxModel) {
		this.boxModel = boxModel;
	}

	public JTextField getTextField_catnum() {
		return textField_catnum;
	}

	public void setTextField_catnum(JTextField textField_catnum) {
		this.textField_catnum = textField_catnum;
	}

	public JTextField getTextField_name() {
		return textField_name;
	}

	public void setTextField_name(JTextField textField_name) {
		this.textField_name = textField_name;
	}

	public JFormattedTextField getTextField_Date() {
		return textField_Date;
	}

	public void setTextField_Date(JFormattedTextField textField_Date) {
		this.textField_Date = textField_Date;
	}

	public JTextField getTextField_gender() {
		return textField_gender;
	}

	public void setTextField_gender(JTextField textField_gender) {
		this.textField_gender = textField_gender;
	}

	public JTextField getTextField_species() {
		return textField_species;
	}

	public void setTextField_species(JTextField textField_species) {
		this.textField_species = textField_species;
	}

	public JTextField getTextField_color() {
		return textField_color;
	}

	public void setTextField_color(JTextField textField_color) {
		this.textField_color = textField_color;
	}

	public JTextField getTextField_price() {
		return textField_price;
	}

	public void setTextField_price(JTextField textField_price) {
		this.textField_price = textField_price;
	}

	public JTextField getTextField_supplydate() {
		return textField_supplydate;
	}

	public void setTextField_supplydate(JTextField textField_supplydate) {
		this.textField_supplydate = textField_supplydate;
	}

	public DefaultListModel getScatlistModel() {
		return scatlistModel;
	}

	public void setScatlistModel(DefaultListModel scatlistModel) {
		this.scatlistModel = scatlistModel;
	}

	public JList getCatList() {
		return catList;
	}

	public void setCatList(JList catList) {
		this.catList = catList;
	}
	
	public DefaultListModel getListModel() {
		return listModel;
	}

	public void setListModel(DefaultListModel listModel) {
		this.listModel = listModel;
	}


	@Override
	public void modelPropertyChange(PropertyChangeEvent evt) {
		// TODO Auto-generated method stub
		
	}
}
