package View;

import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;

import Controller.*;
import Database.DbManager;

import javax.swing.JTabbedPane;
import javax.swing.BoxLayout;
import javax.swing.JList;
import java.awt.GridLayout;
import java.util.Date;
import java.util.Vector;
import java.awt.FlowLayout;
import java.awt.Button;

import Model.*;

import javax.swing.AbstractListModel;
import java.awt.Scrollbar;
import javax.swing.JScrollPane;
import javax.swing.JButton;
import javax.swing.ListSelectionModel;
import javax.swing.JTable;
import javax.swing.JTextField;

public class MainView extends JFrame {

	private JPanel contentPane;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					DbManager.getInstance().Initialize("story", "story");
					DbManager.getInstance().Connect();
					MainView frame = new MainView();
					frame.setVisible(true);
				//	DbManager.getInstance().Finalize();
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public MainView() {		
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 900, 481);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		contentPane.setLayout(new BorderLayout(0, 0));
		setContentPane(contentPane);
		
		JTabbedPane tabbedPane = new JTabbedPane(JTabbedPane.TOP);
		contentPane.add(tabbedPane, BorderLayout.CENTER);
		
		CatModel catModel = new CatModel();
		SellingCatModel sellingcatModel = new SellingCatModel();
		CatPanel CatPanel = new CatPanel(catModel, sellingcatModel);
		
		CatController catController = new CatController(CatPanel, catModel);
		SellingCatController sellingCatController = new SellingCatController(sellingcatModel, CatPanel);
		
		tabbedPane.addTab("Cat", null, CatPanel, null);
		CatPanel.setLayout(null);

		CatteryModel catteryModel = new CatteryModel();
		CatteryPanel CatteryPanel = new CatteryPanel(catteryModel);
		CatteryController catteryController = new CatteryController(catteryModel, CatteryPanel);

		tabbedPane.addTab("Cattery", null, CatteryPanel, null);
		CatteryPanel.setLayout(null);
		

		
		// public Cat(int catnumber, String name, Date birthday, int sex, String kind, String color, String cost) {
		
		HospitalModel hospitalModel = new HospitalModel();
		HospitalView HospitalPanel = new HospitalView(hospitalModel);
		HospitalController hospitalController = new HospitalController(hospitalModel, HospitalPanel);
		
		tabbedPane.addTab("Hospital", null, HospitalPanel, null);
		
		AdopterModel adopterModel = new AdopterModel();
		AdopterPanel AdopterPanel = new AdopterPanel(adopterModel);
		AdopterController adopterController = new AdopterController(adopterModel, AdopterPanel);

		tabbedPane.addTab("Adopter", null, AdopterPanel, null);
		
		
		CatVaccinModel catvaccinModel = new CatVaccinModel();
		CatVaccinPanel CatVaccinPanel = new CatVaccinPanel(catvaccinModel);
		CatVaccinController catvaccinController = new CatVaccinController(catvaccinModel, CatVaccinPanel);		
		tabbedPane.addTab("Cat Vaccin", null, CatVaccinPanel, null);
	}
}
