package View;

import java.util.Vector;

import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.table.DefaultTableModel;

import Model.CatVaccinModel;
import javax.swing.ListSelectionModel;
import javax.swing.DefaultComboBoxModel;
import javax.swing.JComboBox;
import javax.swing.JTextField;
import javax.swing.JButton;

public class CatVaccinPanel extends JPanel {
	private CatVaccinModel model;
	
	private Vector<String> cvTableColumn = new Vector<>();
	private DefaultTableModel cvTableModel;
	private JTable catvaccinTable;
	private JTextField textField_Date;
	
	private DefaultComboBoxModel<String> catnumBoxModel = new DefaultComboBoxModel<>();
	private DefaultComboBoxModel<String> vacnumBoxModel = new DefaultComboBoxModel<>();
	private Vector<String> catnumTableColumn = new Vector<>();
	private DefaultTableModel catnumTableModel;
	private JButton addButton;
	private JButton queryButton;
	private JTextField textField_vaccinnum;
	private JTable table_catList;

	/**
	 * Create the panel.
	 */
	public CatVaccinPanel(CatVaccinModel m) {
		model = m;
		
		setLayout(null);
		
		JScrollPane catvaccinScrollPanel = new JScrollPane();
		catvaccinScrollPanel.setBounds(241, 43, 240, 302);
		add(catvaccinScrollPanel);
		
		cvTableColumn.addElement("Cat num");
		cvTableColumn.addElement("Vaccin num");
		cvTableColumn.addElement("vDate");
		cvTableModel = new DefaultTableModel(cvTableColumn, 0) {
		    @Override
		    public boolean isCellEditable(int row, int column) {
		       //all cells false
		       return false;
		    }
		};
		
		catvaccinTable = new JTable(cvTableModel);
		catvaccinTable.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
		catvaccinScrollPanel.setViewportView(catvaccinTable);
		
		JComboBox comboBox_Catnumber = new JComboBox();
		comboBox_Catnumber.setBounds(26, 71, 168, 24);
		comboBox_Catnumber.setModel(catnumBoxModel);
		add(comboBox_Catnumber);
		
		JComboBox comboBox_VaccinNumber = new JComboBox();
		comboBox_VaccinNumber.setBounds(26, 176, 168, 24);
		comboBox_VaccinNumber.setModel(vacnumBoxModel);
		add(comboBox_VaccinNumber);
		
		textField_Date = new JTextField();
		textField_Date.setBounds(26, 269, 168, 24);
		add(textField_Date);
		textField_Date.setColumns(10);
		
		addButton = new JButton("\uC811\uC885 \uCD94\uAC00");
		addButton.setBounds(66, 345, 105, 27);
		add(addButton);
		
		queryButton = new JButton("\uC870\uD68C");
		queryButton.setBounds(579, 83, 105, 27);
		add(queryButton);
		
		textField_vaccinnum = new JTextField();
		textField_vaccinnum.setBounds(495, 39, 291, 24);
		add(textField_vaccinnum);
		textField_vaccinnum.setColumns(10);
		
		catnumTableColumn.addElement("Cat Num");
		catnumTableColumn.addElement("Cat Name");
		catnumTableModel = new DefaultTableModel(catnumTableColumn, 0) {
		    @Override
		    public boolean isCellEditable(int row, int column) {
		       //all cells false
		       return false;
		    }
		};
		
		table_catList = new JTable();
		table_catList.setBounds(495, 122, 291, 330);
		table_catList.setModel(catnumTableModel);
		add(table_catList);

	}

	public DefaultTableModel getCatnumTableModel() {
		return catnumTableModel;
	}

	public void setCatnumTableModel(DefaultTableModel catnumTableModel) {
		this.catnumTableModel = catnumTableModel;
	}

	public JTextField getTextField_Date() {
		return textField_Date;
	}

	public void setTextField_Date(JTextField textField_Date) {
		this.textField_Date = textField_Date;
	}

	public JButton getQueryButton() {
		return queryButton;
	}

	public void setQueryButton(JButton queryButton) {
		this.queryButton = queryButton;
	}

	public JTextField getTextField_vaccinnum() {
		return textField_vaccinnum;
	}

	public void setTextField_vaccinnum(JTextField textField_vaccinnum) {
		this.textField_vaccinnum = textField_vaccinnum;
	}

	public JTable getTable_catList() {
		return table_catList;
	}

	public void setTable_catList(JTable table_catList) {
		this.table_catList = table_catList;
	}

	public JTable getCatvaccinTable() {
		return catvaccinTable;
	}

	public void setCatvaccinTable(JTable catvaccinTable) {
		this.catvaccinTable = catvaccinTable;
	}

	public DefaultComboBoxModel<String> getCatnumBoxModel() {
		return catnumBoxModel;
	}

	public void setCatnumBoxModel(DefaultComboBoxModel<String> catnumBoxModel) {
		this.catnumBoxModel = catnumBoxModel;
	}

	public DefaultComboBoxModel<String> getVacnumBoxModel() {
		return vacnumBoxModel;
	}

	public void setVacnumBoxModel(DefaultComboBoxModel<String> vacnumBoxModel) {
		this.vacnumBoxModel = vacnumBoxModel;
	}

	public JButton getAddButton() {
		return addButton;
	}

	public void setAddButton(JButton addButton) {
		this.addButton = addButton;
	}

	public DefaultTableModel getCvTableModel() {
		return cvTableModel;
	}

	public void setCvTableModel(DefaultTableModel cvTableModel) {
		this.cvTableModel = cvTableModel;
	}
}
